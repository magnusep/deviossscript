#!/bin/bash
echo "userdata running on hostname: $(uname -n)"

echo "Registering instance to RHN"
subscription-manager register --username petr.magnusek@t-mobile.cz --password panippanip --auto-attach

echo "Adding EPEL Repository"
rpm -iUvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm


echo "Installing pip and git"
sudo yum -y update
sudo yum install -y python-devel python-pip git gcc

echo "userdata running on hostname: $(uname -n)"
echo "Using pip to install Ansible"
sudo pip install --upgrade ansible 2>&1

echo "Cloning repo with Ansible playbook"
git clone https://magnusep:@bitbucket.org/magnusep/deviossscript.git /tmp/app


pushd /tmp/app
ansible-playbook /tmp/app/devTTapp/playbook.yml
popd
exit 0
